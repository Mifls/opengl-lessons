#ifndef OPENGL_LESSONS_INPUT_LISTENER_H
#define OPENGL_LESSONS_INPUT_LISTENER_H

#include <X11/extensions/XInput2.h>
#include "../XWindow.h"

class XListener{
public:
    XIEventMask *mask;
    XGenericEventCookie *cookie;

    XListener(XWindow *window){
        mask = new XIEventMask;
        mask->deviceid = XIAllDevices;
        mask->mask_len = XIMaskLen(XI_LASTEVENT);
        mask->mask = (unsigned char*)calloc(mask->mask_len, sizeof(char));

        XISetMask(mask->mask, XI_RawKeyPress);
        XISetMask(mask->mask, XI_RawKeyRelease);
        XISetMask(mask->mask, XI_RawMotion);

        XISelectEvents(window->display, DefaultRootWindow(window->display), mask, 1);

        cookie = (XGenericEventCookie*)&window->event.xcookie;
    }
};

#endif //OPENGL_LESSONS_INPUT_LISTENER_H
