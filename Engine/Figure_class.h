#ifndef OPENGL_LESSONS_FIGURE_CLASS_H
#define OPENGL_LESSONS_FIGURE_CLASS_H

#include <GL/gl.h>

class Figure{
public:
    GLfloat *vertices;
    GLuint *indices;
    int vertexCount, indexCount;
    GLuint texture;

    //Buffers
    GLuint VBO, VAO, EBO;

    void InitBuffers(){
        //Init buffers
        glGenBuffers(1, &this->VBO);
        glGenVertexArrays(1, &this->VAO);
        glGenBuffers(1, &this->EBO);

        glBindVertexArray(this->VAO);
        // 2. Копируем наши вершины в буфер для OpenGL
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*this->vertexCount, this->vertices, GL_STATIC_DRAW);
        // 3. Копируем наши индексы в в буфер для OpenGL
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*this->indexCount, this->indices, GL_STATIC_DRAW);
        // 3. Устанавливаем указатели на вершинные атрибуты
        glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2, 2, GL_FLOAT,GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
        glEnableVertexAttribArray(2);
        // 4. Отвязываем VAO (НЕ EBO)
        glBindVertexArray(0);
    }

    void drawFigure(){
        glBindTexture(GL_TEXTURE_2D,this->texture);
        glBindVertexArray(this->VAO);
        glDrawElements(GL_TRIANGLES, this->indexCount, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }

    void CleanBuffers(){
        glDeleteVertexArrays(1, &this->VAO);
        glDeleteBuffers(1, &this->VBO);
        glDeleteBuffers(1, &this->EBO);
    }
};

#endif //OPENGL_LASSONS_FIGURE_CLASS_H
