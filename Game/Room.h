#ifndef OPENGL_LESSONS_ROOM_H
#define OPENGL_LESSONS_ROOM_H


#include "../Engine/Texture.h"
#include "../Engine/Figure_class.h"
#include <cstring>

class Room {
public:
    Figure floorFigure;
    Figure ceilFigure;
    Figure wallFigure;

    Room() {
        //floor
        floorFigure.texture = LoadTexture(strdup("../Resources/Textures/floor.jpg"));
        floorFigure.vertices = new GLfloat[32]{
                // Позиции          // Цвета             // Текстурные координаты
                450.f, 0.f, 450.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,   // Верхний правый
                450.f, 0.f, -450.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,   // Нижний правый
                -450.f, 0.f, -450.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,   // Нижний левый
                -450.f, 0.f, 450.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f    // Верхний левый
        };
        floorFigure.vertexCount = 32;

        floorFigure.indices = new GLuint[6]{  // Помните, что мы начинаем с 0!
                0, 1, 3,   // Первый треугольник
                1, 2, 3    // Второй треугольник
        };
        floorFigure.indexCount = 6;

        //ceil
        ceilFigure.texture = LoadTexture(strdup("../Resources/Textures/ceil.jpg"));
        ceilFigure.vertices = new GLfloat[32]{
                // Позиции          // Цвета             // Текстурные координаты
                450.f, 250.f, 450.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,   // Верхний правый
                450.f, 250.f, -450.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,   // Нижний правый
                -450.f, 250.f, -450.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,   // Нижний левый
                -450.f, 250.f, 450.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f    // Верхний левый
        };
        ceilFigure.vertexCount = 32;

        ceilFigure.indices = new GLuint[6]{  // Помните, что мы начинаем с 0!
                0, 3, 1,   // Первый треугольник
                1, 3, 2    // Второй треугольник
        };
        ceilFigure.indexCount = 6;

        //wall
        wallFigure.texture = LoadTexture(strdup("../Resources/Textures/wall.jpg"));
        wallFigure.vertices = new GLfloat[160]{
                // Позиции          // Цвета             // Текстурные координаты
                450.f, 250.f, 80.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,   // Верхний правый
                450.f, 0.f, 80.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,   // Нижний правый
                350.f, 0.f, 80.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,   // Нижний левый
                350.f, 250.f, 80.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,    // Верхний левый

                350.f, 250.f, 350.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,   // Верхний правый
                350.f, 0.f, 350.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,   // Нижний правый
                350.f, 0.f, 80.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,   // Нижний левый
                350.f, 250.f, 80.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,    // Верхний левый

                350.f, 250.f, 350.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,   // Верхний правый
                350.f, 0.f, 350.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,   // Нижний правый
                80.f, 0.f, 350.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,   // Нижний левый
                80.f, 250.f, 350.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,    // Верхний левый

                80.f, 250.f, 450.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,   // Верхний правый
                80.f, 0.f, 450.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,   // Нижний правый
                80.f, 0.f, 350.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,   // Нижний левый
                80.f, 250.f, 350.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,    // Верхний левый

                80.f, 250.f, 444.99f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,   // Верхний правый
                80.f, 210.f, 444.99f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,   // Нижний правый
                -80.f, 210.f, 444.99f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,   // Нижний левый
                -80.f, 250.f, 444.99f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f    // Верхний левый
        };
        wallFigure.vertexCount = 160;

        wallFigure.indices = new GLuint[30]{  // Помните, что мы начинаем с 0!
                0, 1, 3,   // Первый треугольник
                1, 2, 3,    // Второй треугольник

                4, 7, 5,   // Первый треугольник
                5, 7, 6,    // Второй треугольник

                8, 9, 11,   // Первый треугольник
                9, 10, 11,    // Второй треугольник

                12, 15, 13,   // Первый треугольник
                13, 15, 14,    // Второй треугольник

                16, 17, 19,   // Первый треугольник
                17, 18, 19,    // Второй треугольник
        };
        wallFigure.indexCount = 30;
        initBf();
    }

    void initBf(){
        this->floorFigure.InitBuffers();
        this->ceilFigure.InitBuffers();
        this->wallFigure.InitBuffers();
    }
};


#endif //OPENGL_LESSONS_ROOM_H
