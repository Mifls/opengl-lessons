#ifndef OPENGL_LESSONS_DOOR_H
#define OPENGL_LESSONS_DOOR_H

#include "../Engine/Texture.h"
#include "../Engine/Figure_class.h"
#include <cstring>

class Door {
public:
    Figure doorFigure;
    Figure doorFigure1;

    int activeDoor = 0;
    int doorState = 0;

    Door() {
        //floor
        doorFigure.texture = LoadTexture(strdup("../Resources/Textures/iron.jpg"));
        doorFigure.vertices = new GLfloat[144]{
                // Позиции          // Цвета             // Текстурные координаты
                -80.f, 180.f, 445.05f, 0.0f, 1.0f, 0.0f, 1.0f, 0.14286f, //0
                -80.f, 210.f, 445.05f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,     //1
                80.f, 210.f, 445.05f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,      //2
                80.f, 180.f, 445.05f, 0.0f, 0.0f, 1.0f, 0.0f, 0.14286f,  //3

                -80.f, 0.f, 445.05f, 0.0f, 1.0f, 0.0f, 1.0f, 1.f,        //4
                -80.f, 30.f, 445.05f, 0.0f, 1.0f, 0.0f, 1.0f, 1-0.14286f,//5
                80.f, 30.f, 445.05f, 0.0f, 0.0f, 1.0f, 0.0f, 1-0.14286f, //6
                80.f, 0.f, 445.05f, 0.0f, 0.0f, 1.0f, 0.0f, 1.f,         //7

                -80.f, 180.f, 445.05f, 0.0f, 1.0f, 0.0f, 1.f, 0.14286f,  //8
                -60.f, 180.f, 445.05f, 0.0f, 0.0f, 1.0f, .875f, 0.14286f,//9
                -60.f, 30.f, 445.05f, 0.0f, 0.0f, 1.0f, .875f, 1-0.14286f,//10

                80.f, 180.f, 445.05f, 0.0f, 1.0f, 0.0f, 0.f, 0.14286f,
                60.f, 180.f, 445.05f, 0.0f, 0.0f, 1.0f, .125f, 0.14286f,
                60.f, 30.f, 445.05f, 0.0f, 0.0f, 1.0f, .125f, 1-0.14286f,

                53.f, 37.f, 449.99f, 0.0f, 1.0f, 0.0f, 0.16875f, 1-0.1762f,
                53.f, 173.f, 449.99f, 0.0f, 1.0f, 0.0f, 0.16875f, 0.1762f,//15
                -53.f, 173.f, 449.99f, 0.0f, 0.0f, 1.0f, 1-0.16875f, 0.1762f,//16
                -53.f, 37.f, 449.99f, 0.0f, 0.0f, 1.0f, 1-0.16875f, 1-0.1762f,//17
        };
        doorFigure.vertexCount = 144;

        doorFigure.indices = new GLuint[48]{  // Помните, что мы начинаем с 0!
                0, 1, 3,   // Первый треугольник
                1, 2, 3,    // Второй треугольник

                4, 5, 7,   // Первый треугольник
                5, 6, 7,    // Второй треугольник

                8, 10, 5,   // Первый треугольник
                9, 10, 8,    // Второй треугольник

                6, 13, 11,   // Первый треугольник
                11, 13, 12,    // Второй треугольник

                14, 10, 17,   // Первый треугольник
                10, 14, 13,    // Второй треугольник

                9, 16, 17,   // Первый треугольник
                10, 9, 17,    // Второй треугольник

                12, 14, 15,   // Первый треугольник
                12, 13, 14,    // Второй треугольник

                12, 15, 16,   // Первый треугольник
                16, 9, 12,    // Второй треугольник
        };
        doorFigure.indexCount = 48;

        doorFigure1.texture = LoadTexture(strdup("../Resources/Textures/metal.jpg"));
        doorFigure1.vertices = new GLfloat[32]{
                // Позиции          // Цвета             // Текстурные координаты
                53.f, 37.f, 449.99f, 0.0f, 1.0f, 0.0f, 0.16875f, 1-0.1762f,
                53.f, 173.f, 449.99f, 0.0f, 1.0f, 0.0f, 0.16875f, 0.1762f,//15
                -53.f, 173.f, 449.99f, 0.0f, 0.0f, 1.0f, 1-0.16875f, 0.1762f,//16
                -53.f, 37.f, 449.99f, 0.0f, 0.0f, 1.0f, 1-0.16875f, 1-0.1762f,//17
        };
        doorFigure1.vertexCount = 32;

        doorFigure1.indices = new GLuint[6]{  // Помните, что мы начинаем с 0!
                0, 3, 1,   // Первый треугольник
                1, 3, 2,    // Второй треугольник

        };
        doorFigure1.indexCount = 6;
    }

    void initBf(){
        this->doorFigure.InitBuffers();
        this->doorFigure1.InitBuffers();
    }

};

#endif //OPENGL_LESSONS_DOOR_H
