#ifndef OPENGL_LESSONS_XWINDOW_H
#define OPENGL_LESSONS_XWINDOW_H

#include <GL/gl.h>
#include <GL/glx.h>
#include <cstdlib>
#include <cerrno>
#include <X11/Xlib.h>
#include <cstring>
#include <cstdio>
#include <iostream>

class XWindow {
public:
    Display *display;
    Window window;
    Window rootWin;
    Screen*  screen;
    XEvent event;
    int screenId;
    XWindowAttributes attribs;
    GC gc;
    bool created = false;
    int Width = 1000, Height = 700;

    XWindow() {

        if ((display = XOpenDisplay(getenv("DISPLAY"))) == NULL) {  // Соединиться с X сервером,
            printf("Can't connect X server: %s\n", strerror(errno));
            return;
        }
        this->screen = DefaultScreenOfDisplay(this->display);
        this->screenId = DefaultScreen(this->display);
        this->rootWin = DefaultRootWindow(this->display);

        // Check GLX version
        GLint majorGLX, minorGLX = 0;
        glXQueryVersion(this->display, &majorGLX, &minorGLX);
        if (majorGLX <= 1 && minorGLX < 2) {
            printf("GLX 1.2 or greater is required.\n");
            XCloseDisplay(this->display);
            return;
        }
        else {
            printf("GLX version: %d.%d\n", majorGLX, minorGLX);
        }

        // GLX, create XVisualInfo, this is the minimum visuals we want
        GLint glxAttribs[] = {
                GLX_RGBA,
                GLX_DOUBLEBUFFER,
                GLX_DEPTH_SIZE,     24,
                GLX_STENCIL_SIZE,   8,
                GLX_RED_SIZE,       8,
                GLX_GREEN_SIZE,     8,
                GLX_BLUE_SIZE,      8,
                GLX_SAMPLE_BUFFERS, 0,
                GLX_SAMPLES,        0,
                None
        };
        XVisualInfo* visual = glXChooseVisual(this->display, this->screenId, glxAttribs);

        if (visual == 0) {
            printf("Could not create correct visual window.\n");
            XCloseDisplay(this->display);
            return;
        }

        // Open the window
        XSetWindowAttributes windowAttribs;
        windowAttribs.border_pixel = BlackPixel(this->display, this->screenId);
        windowAttribs.background_pixel = WhitePixel(this->display, this->screenId);
        windowAttribs.override_redirect = True;
        windowAttribs.colormap = XCreateColormap(this->display, this->rootWin, visual->visual, AllocNone);
        windowAttribs.event_mask = ExposureMask | KeyPressMask;
        window = XCreateWindow(this->display, this->rootWin, 0, 0, this->Width, this->Height, 0, visual->depth, InputOutput, visual->visual, CWBackPixel | CWColormap | CWBorderPixel | CWEventMask, &windowAttribs);

        // Create GLX OpenGL context
        GLXContext context = glXCreateContext(this->display, visual, NULL, GL_TRUE);
        glXMakeCurrent(this->display, this->window, context);

        printf("GL Vendor: %s\n", glGetString(GL_VENDOR));
        printf("GL Renderer: %s\n", glGetString(GL_RENDERER));
        printf("GL Version: %s\n", glGetString(GL_VERSION));
        printf("GL Shading Language: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

        // Show the window
        XClearWindow(this->display, this->window);
        XMapRaised(this->display, this->window);

        created = true;
    }

    void updateAttribs(){
        XGetWindowAttributes(this->display, this->window, &this->attribs);
    }

    Cursor makeBlankCursor(void) {
        static char data[1] = {0};
        Cursor cursor;
        Pixmap blank;
        XColor dummy;

        blank = XCreateBitmapFromData(this->display, this->rootWin, data, 1, 1);
        cursor = XCreatePixmapCursor(this->display, blank, blank, &dummy, &dummy, 0, 0);
        XFreePixmap(this->display, blank);
        XDefineCursor(this->display, this->window, cursor);

        return cursor;
    }
};

#endif //OPENGL_LESSONS_XWINDOW_H