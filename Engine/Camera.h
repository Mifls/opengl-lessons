#ifndef OPENGL_LESSONS_CAMERA_H
#define OPENGL_LESSONS_CAMERA_H

#include "../stdafx.h"

class Camera{
public:
    glm::vec3 cameraPos;
    glm::vec3 cameraFront;
    glm::vec3 cameraUp;

    glm::mat4 view;

    Camera(){
        cameraPos   = glm::vec3(0.0f, 165.0f,  0.0f);
        cameraFront = glm::vec3(0.0f, 0.0f, 1.0f);
        cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

        view = glm::mat4(1.0f);
    }

    void updateView(){
        view = glm::lookAt( cameraPos, cameraPos + cameraFront, cameraUp);
    }
};
#endif //OPENGL_LESSONS_CAMERA_H
