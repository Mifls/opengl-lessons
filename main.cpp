#include "stdafx.h"
#include "XWindow.h"
#include "Engine/Shaders.h"
#include "Game/Room.h"
#include "Engine/Utils.h"
#include "Engine/Camera.h"
#include "Engine/Input_listener.h"
#include "Engine/Text.h"
#include "Game/Door.h"


extern int errno;

XWindow *window;
Room *room;
Door *door;
Timer *timer;
Camera *camera;

TextRenderer *tr;

float cameraSpeed = 0.005f;

XListener *listener;

//Matrices
glm::mat4 model;
glm::mat4 *doorModel;
glm::mat4 *roomModel;
glm::mat4 projection;

//XInput2
XIDeviceEvent *deviceEvent;

glm::vec3 forward;
glm::vec3 walk(0.f, 0.f, 0.f);
glm::vec3 doorUp;

bool working = true;
bool moving[4] = {false, false, false, false};
bool doorMoving;

//FPS
FPScounter *fpsCounter;

int Init();
int Terminate();

int InitGame();

int main( ) {
    Init();
    InitGame();

    ShaderProgram *shaderProgram = new ShaderProgram("../Resources/Shaders/Simple3DVertexShader.sdr","../Resources/Shaders/Simple3DFragmentShader.sdr");
    shaderProgram->loadFromFile();

    ShaderProgram *textShader = new ShaderProgram("../Resources/Shaders/TextVertexShader.sdr","../Resources/Shaders/TextFragmentShader.sdr");
    textShader->loadFromFile();

    room->initBf();
    door->initBf();

    camera->updateView();
    //projection = glm::ortho(0.0f, 800.0f, 0.0f, 600.0f);
    projection = glm::perspective(45.0f, (GLfloat)window->Width / (GLfloat)window->Height, 0.1f, 1000.0f);

    // Get their uniform location
    GLint modelLoc = glGetUniformLocation(shaderProgram->ProgramID, "model");
    GLint viewLoc = glGetUniformLocation(shaderProgram->ProgramID, "view");
    GLint projLoc = glGetUniformLocation(shaderProgram->ProgramID, "projection");

    timer->Reset();
    while( working ) {                                        // Бесконечный цикл обработки событий
        while (XPending(window->display)>0) {
            XNextEvent(window->display, &window->event);

            if (XGetEventData(window->display, listener->cookie) && listener->cookie->type == GenericEvent) {
                switch (listener->cookie->evtype) {
                    case XI_RawKeyPress: {
                        deviceEvent = (XIDeviceEvent *) listener->cookie->data;
                        if(deviceEvent->detail == 25){
                            moving[0] = true;
                        }
                        if(deviceEvent->detail == 39){
                            moving[2] = true;
                        }
                        if(deviceEvent->detail == 40){
                            moving[1] = true;
                        }
                        if(deviceEvent->detail == 38){
                            moving[3] = true;
                        }
                        //std::cout << "found XI_KeyPress event: keycode " << deviceEvent->detail << "\n";
                        break;
                    }
                    case XI_RawKeyRelease: {
                        deviceEvent = (XIDeviceEvent *) listener->cookie->data;
                        if(deviceEvent->detail == 25) moving[0] = false;
                        if(deviceEvent->detail == 39) moving[2] = false;
                        if(deviceEvent->detail == 40) moving[1] = false;
                        if(deviceEvent->detail == 38) moving[3] = false;
                        break;
                    }
                    case XI_RawMotion: {
                        deviceEvent = (XIDeviceEvent *) listener->cookie->data;
                        vectorRotate(&camera->cameraFront, cameraSpeed * (deviceEvent->event_x), -cameraSpeed * (deviceEvent->event_y) , 1.5f);
                        break;
                    }
                }
            }

            XFreeEventData(window->display, listener->cookie);
            switch (window->event.type)
            {
                // тут обрабатываешь события
                case Expose: {                         // Перерисовать окно
                    window->updateAttribs();
                    glViewport(0, 0, window->attribs.width, window->attribs.height);
                    projection = glm::perspective(45.0f,
                                                  (GLfloat)window->attribs.width / (GLfloat)window->attribs.height,
                                                  0.1f,
                                                  3000.0f);
                    tr->setProjection(glm::ortho(0.0f, static_cast<GLfloat>(window->attribs.width),
                                                 0.0f, static_cast<GLfloat>(window->attribs.height)));
                    break;
                }
                case KeyPress: {
                    if ( window->event.xkey.keycode == 0x09 ) working = false; //Выход по Esc
                    break;
                }
                default: break;
            }
        }
        //glViewport(0, 0, window->Width, window->Height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Using shader
        shaderProgram->Use();

        fpsCounter->cutoff(timer->countDiff());

        walk = glm::vec3(0.f);
        if(moving[0])walk.x+=1.f;
        if(moving[1])walk.z+=1.f;
        if(moving[2])walk.x-=1.f;
        if(moving[3])walk.z-=1.f;

        if(vectorNormalize(&walk)){
            //camera->cameraPos+= camera->cameraFront*(0.0000005f * timer->countDiff());
            forward.x = camera->cameraFront.x*walk.x - camera->cameraFront.z*walk.z;
            forward.z = camera->cameraFront.x*walk.z + camera->cameraFront.z*walk.x;
            forward.y = 0;
            vectorNormalize(&forward);
            forward = camera->cameraPos + forward*(0.0000005f * timer->lastDiff);

            while(forward.x>450.1f){
                forward.x-=900.f;
            }
            while(forward.x<-450.1f){
                forward.x+=900.f;
            }
            while(forward.z>450.1f){
                forward.z-=900.f;
            }
            while(forward.z<-450.1f){
                forward.z+=900.f;
            }

            doorMoving = true;
            if(forward.x > 343.f){
                if(std::abs(camera->cameraPos.z) <= 73.f){
                    if(std::abs(forward.z) > 73.f){
                        if(forward.z > 0) forward.z = 73.f;
                        else forward.z = -73.f;
                    }
                    door->activeDoor = 1;
                    door->doorState = 1;
                    doorMoving = false;
                } else forward.x = 342.999f;
            }
            if(forward.x < -343.f){
                if(std::abs(camera->cameraPos.z) <= 73.f){
                    if(std::abs(forward.z) > 73.f){
                        if(forward.z > 0) forward.z = 73.f;
                        else forward.z = -73.f;
                    }
                    door->activeDoor = 3;
                    door->doorState = 1;
                    doorMoving = false;
                } else forward.x = -342.999f;
            }
            if(forward.z > 343.f){
                if(std::abs(camera->cameraPos.x) <= 73.f){
                    if(std::abs(forward.x) > 73.f){
                        if(forward.x > 0) forward.x = 73.f;
                        else forward.x = -73.f;
                    }
                    door->activeDoor = 0;
                    door->doorState = 1;
                    doorMoving = false;
                } else forward.z = 342.999f;
            }
            if(forward.z < -343.f){
                if(std::abs(camera->cameraPos.x) <= 73.f){
                    if(std::abs(forward.x) > 73.f){
                        if(forward.x > 0) forward.x = 73.f;
                        else forward.x = -73.f;
                    }
                    door->activeDoor = 2;
                    door->doorState = 1;
                    doorMoving = false;
                } else forward.z = -342.999f;
            }
            if(doorMoving)door->doorState = 2;
            camera->cameraPos = forward;
        }
        camera->updateView();

        switch (door->doorState){
            case 0: break;
            case 1:{
                doorUp.y += 0.0000014f * timer->lastDiff;
                if(doorUp.y>=210.1f){
                    doorUp.y = 210.1f;
                    door->doorState = 0;
                }
                break;
            }
            case 2:{
                doorUp.y -= 0.0000014f * timer->lastDiff;
                if(doorUp.y<=0.f){
                    doorUp.y = 0.f;
                    door->doorState = 0;
                }
                break;
            }
        }
        //usleep(40000);

        // Pass them to the shaders
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camera->view));
        // Note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

        model = glm::mat4(1.0f);

        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        room->floorFigure.drawFigure();
        room->ceilFigure.drawFigure();

        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(roomModel[door->activeDoor]*model));
        room->floorFigure.drawFigure();
        room->ceilFigure.drawFigure();

        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(roomModel[door->activeDoor]*model));
        glBindTexture(GL_TEXTURE_2D, room->wallFigure.texture);
        glBindVertexArray(room->wallFigure.VAO);
        for (int j = 0; j < 4; ++j) {
            model = glm::rotate(model, Pi / 2, glm::vec3(0.0f, 1.0f, 0.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            glDrawElements(GL_TRIANGLES, room->wallFigure.indexCount, GL_UNSIGNED_INT, 0);

            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(roomModel[door->activeDoor] * model));
            glDrawElements(GL_TRIANGLES, room->wallFigure.indexCount, GL_UNSIGNED_INT, 0);

        }
        glBindVertexArray(0);

        for (int j = 0; j < 4; ++j) {

            model = doorModel[j];
            if(j==door->activeDoor) {
                model = glm::translate(model, doorUp);
            }
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            door->doorFigure.drawFigure();
            door->doorFigure1.drawFigure();

            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(roomModel[door->activeDoor] * glm::rotate(glm::mat4(1.0f), Pi, glm::vec3(0.0f, 1.0f, 0.0f)) * model));
            door->doorFigure.drawFigure();
            door->doorFigure1.drawFigure();
        }

        tr->RenderText(textShader, "fps: " + std::to_string(fpsCounter->curentFPS), 25.0f, 25.0f, 0.3f, glm::vec3(1.f, 1.f, 1.f));
        //tr->RenderText(textShader, "(C) LearnOpenGL.com", 540.0f, 570.0f, 0.5f, glm::vec3(0.3, 0.7f, 0.9f));

        // Present frame
        glFlush();
        glXSwapBuffers(window->display, window->window);

    }


    Terminate();

    return 0;
}

int InitGame(){
    room = new Room();
    door = new Door();
    timer = new Timer();
    fpsCounter = new FPScounter;
    camera = new Camera();
    listener = new XListener(window);
    tr = new TextRenderer("../Fonts/Hack-Bold.ttf", 128);

    doorModel = new glm::mat4[4]{
            glm::mat4(1.0f),
            glm::rotate(glm::mat4(1.0f), Pi / 2, glm::vec3(0.0f, 1.0f, 0.0f)),
            glm::rotate(glm::mat4(1.0f), Pi, glm::vec3(0.0f, 1.0f, 0.0f)),
            glm::rotate(glm::mat4(1.0f), 3 * Pi / 2, glm::vec3(0.0f, 1.0f, 0.0f)),
    };

    roomModel = new glm::mat4[4]{
            glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 900.0f)),
            glm::translate(glm::mat4(1.0f), glm::vec3(900.0f, 0.0f, 0.0f)),
            glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -900.0f)),
            glm::translate(glm::mat4(1.0f), glm::vec3(-900.0f, 0.0f, 0.0f)),
    };

    return 0;
}

int Init(){
    window = new XWindow();
    if(!window->created) exit(-1);

    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

    glViewport(0,0,window->Width,window->Height);
    glMatrixMode(GL_MODELVIEW);

    //Init matrices
    model = glm::mat4(1.0f);
    projection = glm::mat4(1.0f);
    window->makeBlankCursor();

    return 0;
}

int Terminate(){
    //Cleaning figures
    room->floorFigure.CleanBuffers();
    room->ceilFigure.CleanBuffers();
    room->wallFigure.CleanBuffers();
    door->doorFigure.CleanBuffers();
    door->doorFigure1.CleanBuffers();
    tr->CleanBuffers();

    printf("Closing connection...\n");
    XCloseDisplay( window->display );          // Закрыть соединение с X сервером
    return 0;
}