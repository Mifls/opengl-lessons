//
// Created by mifls on 20.02.2020.
//

#ifndef OPENGL_LESSONS_UTILS_H
#define OPENGL_LESSONS_UTILS_H
#include <sys/time.h>
#include <time.h>
#include "../stdafx.h"

class Timer{
public:
    long int lastDiff;
    struct timespec t;
    Timer(){
        Reset();
    }
    long int countDiff(){
        lastDiff = t.tv_nsec + t.tv_sec*1e9;
        if(clock_gettime(CLOCK_MONOTONIC_RAW, &t))return 0;
        lastDiff = t.tv_nsec + t.tv_sec*1e9  - lastDiff;
        return lastDiff;
    }
    void Reset(){
        lastDiff = 0;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t);
    }
};

class FPScounter{
public:
    int counter = 0;
    int frequency = 5;
    long int fpsTime = 0;
    int curentFPS = 0;
    void cutoff(long int diff_ns){
        fpsTime+=diff_ns;
        counter++;
        if(counter == frequency){
            curentFPS = ((1000000000.L / (double)(fpsTime/counter)));
            fpsTime = 0;
            counter = 0;
        }
    }
};

int vectorNormalize(glm::vec3 * Vector){
    float len = sqrt(Vector->x*Vector->x + Vector->y*Vector->y + Vector->z*Vector->z);
    if(len == 0) return 0;
    Vector->x /= len;
    Vector->y /= len;
    Vector->z /= len;
    return 1;
}

void vectorRotate(glm::vec3 * Vector, float A, float h, float maxh){
    float lenA = Vector->x*Vector->x + Vector->z*Vector->z, len = sqrt(Vector->y*Vector->y + lenA), x, y, sn, cs;
    lenA = sqrt(lenA);
    //
    cs = cos(A);
    sn = sin(A);
    x = Vector->x*cs - Vector->z*sn;
    y = Vector->x*sn + Vector->z*cs;
    Vector->x = x;
    Vector->z = y;

    cs = cos(h);
    sn = sin(h);

    x = lenA*cs - Vector->y*sn;
    y = lenA*sn + Vector->y*cs;
    cs = len*cos(maxh);
    if(x - cs< -0.00001f){
        x = cs;
        y = len*sin(maxh);
        if(sn<0)y = -y;
    }
    Vector->y = y;
    x /=lenA;
    Vector->x *= x;
    Vector->z *= x;
}

#endif //OPENGL_LESSONS_UTILS_H
